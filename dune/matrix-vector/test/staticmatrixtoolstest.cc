#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/scaledidmatrix.hh>

#include "../addtodiagonal.hh"
#include "../transformmatrix.hh"

using namespace Dune::MatrixVector;

template <class FieldType>
class StaticMatrixToolsTestSuite {
public:
  bool check() {
    bool passed = true;

    passed = passed and checkAddToDiagonal();
    passed = passed and checkAddTransformedMatrix();
    passed = passed and checkTransformMatrix();

    return passed;
  }

private:
  bool checkAddToDiagonal() {
    bool passed = true;

    const FieldType scalar_a = 3;
    const double double_a = 3;

    // SQUARE MATRICES

    // case FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_x = {{1, 2}, {3, 4}},

                                         squareFieldMatrix_check = {{4, 2},
                                                                    {3, 7}};

      addToDiagonal(squareFieldMatrix_x, scalar_a);

      passed = passed and
               myDiff(squareFieldMatrix_x, squareFieldMatrix_check) < 1e-12;
    }
    // case DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_x = {3, 2},
                                         diagonalMatrix_check = {6, 5};

      addToDiagonal(diagonalMatrix_x, scalar_a);

      passed =
          passed and myDiff(diagonalMatrix_x, diagonalMatrix_check) < 1e-12;
    }
    // case SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_x(2),
          scaledIdentityMatrix_check(5);

      addToDiagonal(scaledIdentityMatrix_x, scalar_a);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_x, scaledIdentityMatrix_check) < 1e-12;
    }
    // case with hardwired scalar type as double (as occurring in
    // localassemblers, see also the commment in staticmatrixtools.hh)
    // why does this even compile?
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_x(2),
          scaledIdentityMatrix_check(5);

      addToDiagonal(scaledIdentityMatrix_x, double_a);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_x, scaledIdentityMatrix_check) < 1e-12;
    }

    // RECTANGULAR MATRICES
    {
      Dune::FieldMatrix<FieldType, 2, 4> rectFieldMatrix_x = {{1, 2, 3, 4},
                                                              {5, 6, 7, 8}},

                                         rectFieldMatrix_check = {{4, 2, 3, 4},
                                                                  {5, 9, 7, 8}};

      addToDiagonal(rectFieldMatrix_x, scalar_a);

      passed =
          passed and myDiff(rectFieldMatrix_x, rectFieldMatrix_check) < 1e-12;
    }

    return passed;
  }

  bool checkAddTransformedMatrix() {
    bool passed = true;

    const FieldType scalar_T1 = 3, scalar_T2 = 4, scalar_B = 2;

    const double double_T1 = 3, double_T2 = 4, double_B = 2;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_T1 = {{1, 2},
                                                                     {3, 4}},
                                             squareFieldMatrix_T2 = {{2, 3},
                                                                     {1, 4}},
                                             squareFieldMatrix_B = {{3, 4},
                                                                    {5, 2}};

    const Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_T1 = {1, 2},
                                             diagonalMatrix_T2 = {2, 3},
                                             diagonalMatrix_B = {3, 2};

    const Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_T1(3),
        scaledIdentityMatrix_T2(4), scaledIdentityMatrix_B(2);

    // test default imp by case Matrix += FM^t*Matrix*FM
    {
      Dune::Matrix<Dune::FieldMatrix<FieldType, 1, 1>> squareMatrix_A(2, 2),
          squareMatrix_B(2, 2), squareMatrix_check(2, 2);
      squareMatrix_A = squareMatrix_B = squareMatrix_check = 0;

      squareMatrix_B[0][0] = 3;
      squareMatrix_B[0][1] = 4;
      squareMatrix_B[1][0] = 5;
      squareMatrix_B[1][1] = 2;

      squareMatrix_check[0][0] = 46;
      squareMatrix_check[0][1] = 94;
      squareMatrix_check[1][0] = 68;
      squareMatrix_check[1][1] = 142;

      addTransformedMatrix(squareMatrix_A, squareFieldMatrix_T1, squareMatrix_B,
                           squareFieldMatrix_T2);

      passed = passed and myDiff(squareMatrix_A, squareMatrix_check) < 1e-12;
    }
    // case FM += FM^t*FM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{46, 94}, {68, 142}};

      addTransformedMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                           squareFieldMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*DM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{12, 33}, {20, 50}};

      addTransformedMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                           diagonalMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += DM^t*FM*DM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{6, 12}, {20, 12}};

      addTransformedMatrix(squareFieldMatrix_A, diagonalMatrix_T1,
                           squareFieldMatrix_B, diagonalMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += DM^t*DM*DM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{6, 0}, {0, 12}};

      addTransformedMatrix(squareFieldMatrix_A, diagonalMatrix_T1,
                           diagonalMatrix_B, diagonalMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*SM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      addTransformedMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                           scaledIdentityMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += SM^t*FM*SM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      addTransformedMatrix(squareFieldMatrix_A, scaledIdentityMatrix_T1,
                           squareFieldMatrix_B, scaledIdentityMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*field_type*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      addTransformedMatrix(squareFieldMatrix_A, squareFieldMatrix_T1, scalar_B,
                           squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += field_type*FM*field_type
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      addTransformedMatrix(squareFieldMatrix_A, scalar_T1, squareFieldMatrix_B,
                           scalar_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*double*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      addTransformedMatrix(squareFieldMatrix_A, squareFieldMatrix_T1, double_B,
                           squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += double*FM*double
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      addTransformedMatrix(squareFieldMatrix_A, double_T1, squareFieldMatrix_B,
                           double_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case DM += DM^t*DM*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {6, 12};

      addTransformedMatrix(diagonalMatrix_A, diagonalMatrix_T1,
                           diagonalMatrix_B, diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*SM*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      addTransformedMatrix(diagonalMatrix_A, diagonalMatrix_T1,
                           scaledIdentityMatrix_B, diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += SM^t*DM*SM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      addTransformedMatrix(diagonalMatrix_A, scaledIdentityMatrix_T1,
                           diagonalMatrix_B, scaledIdentityMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*field_type*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      addTransformedMatrix(diagonalMatrix_A, diagonalMatrix_T1, scalar_B,
                           diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += field_type*DM*field_type
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      addTransformedMatrix(diagonalMatrix_A, scalar_T1, diagonalMatrix_B,
                           scalar_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*double*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      addTransformedMatrix(diagonalMatrix_A, diagonalMatrix_T1, double_B,
                           diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += double*DM*double
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      addTransformedMatrix(diagonalMatrix_A, double_T1, diagonalMatrix_B,
                           double_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case SM += SM^t*SM*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      addTransformedMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1,
                           scaledIdentityMatrix_B, scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += SM^t*field_type*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      addTransformedMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1,
                           scalar_B, scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += field_type*SM*field_type
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      addTransformedMatrix(scaledIdentityMatrix_A, scalar_T1,
                           scaledIdentityMatrix_B, scalar_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += SM^t*double*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      addTransformedMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1,
                           double_B, scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += double*SM*double
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      addTransformedMatrix(scaledIdentityMatrix_A, double_T1,
                           scaledIdentityMatrix_B, double_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case field_type += field_type*field_type*field_type
    {
      FieldType scalar_A(0), scalar_check = 24;

      addTransformedMatrix(scalar_A, scalar_T1, scalar_B, scalar_T2);

      passed = passed and myDiff(scalar_A, scalar_check) < 1e-12;
    }

    // RECTANGULAR MATRICES
    const Dune::FieldMatrix<FieldType, 2, 4> rectFieldMatrix_T1 = {{1, 2, 3, 4},
                                                                   {5, 6, 7,
                                                                    8}},
                                             rectFieldMatrix_T2 = {
                                                 {2, 3, 4, 5}, {6, 7, 8, 9}};

    // case FM += FM^t*FM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{140, 182, 224, 266},
                                   {192, 248, 304, 360},
                                   {244, 314, 384, 454},
                                   {296, 380, 464, 548}};

      addTransformedMatrix(rectFieldMatrix_A, rectFieldMatrix_T1,
                           squareFieldMatrix_B, rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*DM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{66, 79, 92, 105},
                                   {84, 102, 120, 138},
                                   {102, 125, 148, 171},
                                   {120, 148, 176, 204}};

      addTransformedMatrix(rectFieldMatrix_A, rectFieldMatrix_T1,
                           diagonalMatrix_B, rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*SM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      addTransformedMatrix(rectFieldMatrix_A, rectFieldMatrix_T1,
                           scaledIdentityMatrix_B, rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*field_type*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      addTransformedMatrix(rectFieldMatrix_A, rectFieldMatrix_T1, scalar_B,
                           rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*double*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      addTransformedMatrix(rectFieldMatrix_A, rectFieldMatrix_T1, double_B,
                           rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }

    return passed;
  }

  bool checkTransformMatrix() {
    bool passed = true;

    const FieldType scalar_T1 = 3, scalar_T2 = 4, scalar_B = 2;

    const double double_T1 = 3, double_T2 = 4, double_B = 2;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_T1 = {{1, 2},
                                                                     {3, 4}},
                                             squareFieldMatrix_T2 = {{2, 3},
                                                                     {1, 4}},
                                             squareFieldMatrix_B = {{3, 4},
                                                                    {5, 2}};

    const Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_T1 = {1, 2},
                                             diagonalMatrix_T2 = {2, 3},
                                             diagonalMatrix_B = {3, 2};

    const Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_T1(3),
        scaledIdentityMatrix_T2(4), scaledIdentityMatrix_B(2);

    // test default imp by case Matrix += FM^t*Matrix*FM
    {
      Dune::Matrix<Dune::FieldMatrix<FieldType, 1, 1>> squareMatrix_A(2, 2),
          squareMatrix_B(2, 2), squareMatrix_check(2, 2);
      squareMatrix_A = squareMatrix_B = squareMatrix_check = 0;

      squareMatrix_B[0][0] = 3;
      squareMatrix_B[0][1] = 4;
      squareMatrix_B[1][0] = 5;
      squareMatrix_B[1][1] = 2;

      squareMatrix_check[0][0] = 46;
      squareMatrix_check[0][1] = 94;
      squareMatrix_check[1][0] = 68;
      squareMatrix_check[1][1] = 142;

      transformMatrix(squareMatrix_A, squareFieldMatrix_T1, squareMatrix_B,
                      squareFieldMatrix_T2);

      passed = passed and myDiff(squareMatrix_A, squareMatrix_check) < 1e-12;
    }
    // case FM += FM^t*FM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{46, 94}, {68, 142}};

      transformMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                      squareFieldMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*DM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{12, 33}, {20, 50}};

      transformMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                      diagonalMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += DM^t*FM*DM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{6, 12}, {20, 12}};

      transformMatrix(squareFieldMatrix_A, diagonalMatrix_T1,
                      squareFieldMatrix_B, diagonalMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += DM^t*DM*DM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{6, 0}, {0, 12}};

      transformMatrix(squareFieldMatrix_A, diagonalMatrix_T1, diagonalMatrix_B,
                      diagonalMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*SM*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      transformMatrix(squareFieldMatrix_A, squareFieldMatrix_T1,
                      scaledIdentityMatrix_B, squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += SM^t*FM*SM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      transformMatrix(squareFieldMatrix_A, scaledIdentityMatrix_T1,
                      squareFieldMatrix_B, scaledIdentityMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*field_type*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      transformMatrix(squareFieldMatrix_A, squareFieldMatrix_T1, scalar_B,
                      squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += field_type*FM*field_type
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      transformMatrix(squareFieldMatrix_A, scalar_T1, squareFieldMatrix_B,
                      scalar_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*double*FM
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{10, 30}, {16, 44}};

      transformMatrix(squareFieldMatrix_A, squareFieldMatrix_T1, double_B,
                      squareFieldMatrix_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case FM += double*FM*double
    {
      Dune::FieldMatrix<FieldType, 2, 2> squareFieldMatrix_A(0),
          squareFieldMatrix_check = {{36, 48}, {60, 24}};

      transformMatrix(squareFieldMatrix_A, double_T1, squareFieldMatrix_B,
                      double_T2);

      passed = passed and
               myDiff(squareFieldMatrix_A, squareFieldMatrix_check) < 1e-12;
    }
    // case DM += DM^t*DM*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {6, 12};

      transformMatrix(diagonalMatrix_A, diagonalMatrix_T1, diagonalMatrix_B,
                      diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*SM*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      transformMatrix(diagonalMatrix_A, diagonalMatrix_T1,
                      scaledIdentityMatrix_B, diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += SM^t*DM*SM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      transformMatrix(diagonalMatrix_A, scaledIdentityMatrix_T1,
                      diagonalMatrix_B, scaledIdentityMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*field_type*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      transformMatrix(diagonalMatrix_A, diagonalMatrix_T1, scalar_B,
                      diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += field_type*DM*field_type
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      transformMatrix(diagonalMatrix_A, scalar_T1, diagonalMatrix_B, scalar_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += DM^t*double*DM
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {4, 12};

      transformMatrix(diagonalMatrix_A, diagonalMatrix_T1, double_B,
                      diagonalMatrix_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case DM += double*DM*double
    {
      Dune::DiagonalMatrix<FieldType, 2> diagonalMatrix_A(0),
          diagonalMatrix_check = {36, 24};

      transformMatrix(diagonalMatrix_A, double_T1, diagonalMatrix_B, double_T2);

      passed =
          passed and myDiff(diagonalMatrix_A, diagonalMatrix_check) < 1e-12;
    }
    // case SM += SM^t*SM*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      transformMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1,
                      scaledIdentityMatrix_B, scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += SM^t*field_type*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      transformMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1, scalar_B,
                      scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += field_type*SM*field_type
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      transformMatrix(scaledIdentityMatrix_A, scalar_T1, scaledIdentityMatrix_B,
                      scalar_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += SM^t*double*SM
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      transformMatrix(scaledIdentityMatrix_A, scaledIdentityMatrix_T1, double_B,
                      scaledIdentityMatrix_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case SM += double*SM*double
    {
      Dune::ScaledIdentityMatrix<FieldType, 2> scaledIdentityMatrix_A(0),
          scaledIdentityMatrix_check(24);

      transformMatrix(scaledIdentityMatrix_A, double_T1, scaledIdentityMatrix_B,
                      double_T2);

      passed =
          passed and
          myDiff(scaledIdentityMatrix_A, scaledIdentityMatrix_check) < 1e-12;
    }
    // case field_type += field_type*field_type*field_type
    {
      FieldType scalar_A(0), scalar_check = 24;

      transformMatrix(scalar_A, scalar_T1, scalar_B, scalar_T2);

      passed = passed and myDiff(scalar_A, scalar_check) < 1e-12;
    }

    // RECTANGULAR MATRICES
    const Dune::FieldMatrix<FieldType, 2, 4> rectFieldMatrix_T1 = {{1, 2, 3, 4},
                                                                   {5, 6, 7,
                                                                    8}},
                                             rectFieldMatrix_T2 = {
                                                 {2, 3, 4, 5}, {6, 7, 8, 9}};

    // case FM += FM^t*FM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{140, 182, 224, 266},
                                   {192, 248, 304, 360},
                                   {244, 314, 384, 454},
                                   {296, 380, 464, 548}};

      transformMatrix(rectFieldMatrix_A, rectFieldMatrix_T1,
                      squareFieldMatrix_B, rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*DM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{66, 79, 92, 105},
                                   {84, 102, 120, 138},
                                   {102, 125, 148, 171},
                                   {120, 148, 176, 204}};

      transformMatrix(rectFieldMatrix_A, rectFieldMatrix_T1, diagonalMatrix_B,
                      rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*SM*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      transformMatrix(rectFieldMatrix_A, rectFieldMatrix_T1,
                      scaledIdentityMatrix_B, rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*field_type*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      transformMatrix(rectFieldMatrix_A, rectFieldMatrix_T1, scalar_B,
                      rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }
    // case FM += FM^t*double*FM
    {
      Dune::FieldMatrix<FieldType, 4, 4> rectFieldMatrix_A(0),
          rectFieldMatrix_check = {{64, 76, 88, 100},
                                   {80, 96, 112, 128},
                                   {96, 116, 136, 156},
                                   {112, 136, 160, 184}};

      transformMatrix(rectFieldMatrix_A, rectFieldMatrix_T1, double_B,
                      rectFieldMatrix_T2);

      passed =
          passed and myDiff(rectFieldMatrix_A, rectFieldMatrix_check) < 1e-12;
    }

    return passed;
  }

  template <class T>
  double myDiff(T t1, const T& t2) {
    t1 -= t2;
    return t1.infinity_norm();
  }

  double myDiff(FieldType t1, const FieldType& t2) { return std::abs(t1 - t2); }
};

int main(int argc, char* argv[]) {
  Dune::MPIHelper::instance(argc, argv);

  {
    StaticMatrixToolsTestSuite<double> testSuite;
    testSuite.check();
  }
  {
    StaticMatrixToolsTestSuite<float> testSuite;
    testSuite.check();
  }
  {
    StaticMatrixToolsTestSuite<long double> testSuite;
    testSuite.check();
  }
}
