#ifndef DUNE_MATRIX_VECTOR_PROMOTE_HH
#define DUNE_MATRIX_VECTOR_PROMOTE_HH

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

namespace Dune {
namespace MatrixVector {
  // type promotion (smallest matrix that can hold the sum of two matrices
  // *******************
  template <class MatrixA, class MatrixB>
  struct Promote {
    typedef Dune::FieldMatrix<typename MatrixA::field_type, MatrixA::rows,
                              MatrixA::cols>
        Type;
  };

  template <class Matrix>
  struct Promote<Matrix, Matrix> {
    typedef Matrix Type;
  };

  template <typename FieldType, int n>
  struct Promote<Dune::FieldMatrix<FieldType, n, n>,
                 Dune::DiagonalMatrix<FieldType, n>> {
    typedef Dune::FieldMatrix<FieldType, n, n> Type;
  };

  template <typename FieldType, int n>
  struct Promote<Dune::DiagonalMatrix<FieldType, n>,
                 Dune::FieldMatrix<FieldType, n, n>> {
    typedef Dune::FieldMatrix<FieldType, n, n> Type;
  };

  template <typename FieldType, int n>
  struct Promote<Dune::DiagonalMatrix<FieldType, n>,
                 Dune::ScaledIdentityMatrix<FieldType, n>> {
    typedef Dune::DiagonalMatrix<FieldType, n> Type;
  };

  template <typename FieldType, int n>
  struct Promote<Dune::ScaledIdentityMatrix<FieldType, n>,
                 Dune::DiagonalMatrix<FieldType, n>> {
    typedef Dune::DiagonalMatrix<FieldType, n> Type;
  };
}
}

#endif
