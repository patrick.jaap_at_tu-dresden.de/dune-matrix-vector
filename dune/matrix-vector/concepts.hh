#ifndef DUNE_MATRIX_VECTOR_CONCEPTS_HH
#define DUNE_MATRIX_VECTOR_CONCEPTS_HH

// though not needed for this file, checking
// for a concept requires this include anyway
#include <dune/common/concept.hh>

namespace Dune {
namespace MatrixVector {
namespace Concept {

struct HasBegin {
  template <class C>
  auto require(C&& c) -> decltype(c.begin());
};


struct HasN {
  template <class C>
  auto require(C&& c) -> decltype(c.N());
};

struct HasResize {
  template <class C>
  auto require(C&& c) -> decltype(c.resize(0));
};

struct HasSize {
  template <class C>
  auto require(C&& c) -> decltype(c.size());
};

} // end namespace Concept
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_CONCEPTS_HH
