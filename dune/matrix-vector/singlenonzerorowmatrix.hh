// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_SINGLE_NONZERO_ROW_MATRIX_HH
#define DUNE_FUFEM_SINGLE_NONZERO_ROW_MATRIX_HH

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/fufem/arithmetic.hh>
#include <dune/fufem/indexedsliceiterator.hh>

// TODO fix namespace (also for the MatrixTraits injection down below)

/**
 * \brief A static matrix that has only a single nonzero row
 *
 * The number and values of this row are dynamic.
 * For simplicity this derives from FieldMatrix and sets
 * all entries accordingly. So it will not reduce memory
 * requirements but allows to implement methods more efficiently.
 */
template<class K, int ROWS, int COLS> //TODO allow to set storage type as in singlenonzerocolumnmatrix. Update docu accordingly.
class SingleNonZeroRowMatrix
{
public:
    typedef typename Dune::FieldMatrix<K, 1, COLS> SingleRowMatrix;

protected:
    class RowProxy
    {
    public:
        typedef std::size_t size_type;
        typedef typename SingleRowMatrix::ConstColIterator ConstIterator;
        typedef ConstIterator const_iterator;

        RowProxy(const SingleRowMatrix* nzRow, size_type rowIndex, size_type nzRowIndex) :
            nzRow_(nzRow),
            isNonZero_(rowIndex==nzRowIndex)
        {}

        ConstIterator begin() const
        {
            if (isNonZero_)
                return (*nzRow_)[0].begin();
            else
                return (*nzRow_)[0].end();
        }

        ConstIterator end() const
        {
            return (*nzRow_)[0].end();
        }

    protected:
        const SingleRowMatrix* nzRow_;
        bool isNonZero_;
    };

public:
    constexpr static int rows = ROWS;
    constexpr static int cols = COLS;

    typedef RowProxy row_type;
    typedef row_type const_row_reference;
    typedef typename std::size_t size_type;
    typedef typename RowProxy::ConstIterator ConstColIterator;

    /**
     * \brief Create from single row matrix and row index
     */
    SingleNonZeroRowMatrix(const SingleRowMatrix& r, size_type nzRowIndex) :
        nzRow_(r),
        nzRowIndex_(nzRowIndex)
    {}

    size_type N() const
    {
        return ROWS;
    }

    size_type M() const
    {
        return COLS;
    }

    template<class X , class Y >
    void umv(const X& x, Y& y) const
    {
        for(size_type i=0; i<M(); ++i)
            y[nzRowIndex_] += nzRow_[0][i] * x[i];
    }

    template<class X , class Y >
    void umtv(const X& x, Y& y) const
    {
        for(size_type i=0; i<N(); ++i)
            y[i] = nzRow_[0][i] * x[nzRowIndex_];
    }

    template<class X , class Y >
    void mtv(const X& x, Y& y) const
    {
        y = 0.0;
        umtv(x, y);
    }

    const_row_reference operator[] (size_type rowIndex) const
    {
        return const_row_reference(&nzRow_, rowIndex, nzRowIndex_);
    }

    size_type nonZeroRowIndex() const
    {
        return nzRowIndex_;
    }

protected:
    SingleRowMatrix nzRow_;
    const size_type nzRowIndex_;
};


// TODO these are probably outdated.
namespace Arithmetic
{
    template<class K, int ROWS, int COLS>
    struct MatrixTraits<SingleNonZeroRowMatrix<K, ROWS, COLS> >
    {
        constexpr static bool isMatrix = true;
        constexpr static bool isDense = false;
        constexpr static bool sizeIsStatic = true;  // TODO only one of these should be used
        constexpr static bool hasStaticSize = true; // TODO only one of these should be used
        constexpr static int rows = ROWS;
        constexpr static int cols = COLS;
    };
}


#endif
