#ifndef DUNE_MATRIX_VECTOR_TRAITS_MATRIXTRAITS_HH
#define DUNE_MATRIX_VECTOR_TRAITS_MATRIXTRAITS_HH

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

namespace Dune {
namespace MatrixVector {
namespace Traits {

/** \brief Class to identify matrix types and extract information
 *
 * Specialize this class for all types that can be used like a matrix.
 */
template<class T>
struct MatrixTraits {
  constexpr static bool isMatrix = false;
};

template<class T, int n, int m>
struct MatrixTraits<FieldMatrix<T, n, m>> {
  constexpr static bool isMatrix = true;
  constexpr static int rows = n;
  constexpr static int cols = m;
};

template<class T, int n>
struct MatrixTraits<DiagonalMatrix<T, n>> {
  constexpr static bool isMatrix = true;
  constexpr static int rows = n;
  constexpr static int cols = n;
};

template<class T, int n>
struct MatrixTraits<ScaledIdentityMatrix<T, n>> {
  constexpr static bool isMatrix = true;
  constexpr static int rows = n;
  constexpr static int cols = n;
};

template<class T>
struct MatrixTraits<BCRSMatrix<T>> {
  constexpr static bool isMatrix = true;
};

template<class... T>
struct MatrixTraits<MultiTypeBlockMatrix<T...> > {
  constexpr static bool isMatrix = true;
};

} // end namespace Traits
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TRAITS_MATRIXTRAITS_HH
